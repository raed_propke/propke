<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        $gate->define('isAdmin', function($user) {
            return $user->user_type == 'admin';
        });
        
        $gate->define('isTeamMate', function($user) {
            return $user->user_type == 'team-mate';
        });

        $gate->define('isOwner', function($user) {
            return $user->user_type == 'owner';
        });

        $gate->define('roleX', function($user) {
            // dd(1);
            return $user->role == 'x';
        });
    }
}

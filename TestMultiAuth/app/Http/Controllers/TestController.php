<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use User;
use Gate;
class TestController extends Controller
{
    function index()
    {
        // if(!Gate::allows('isAdmin')){
        //     abort(404,'Sorry');
        // }
        // return view('owner');


        // if (Gate::allows('isAdmin')) {
        //     return view('admin');
        // }
        if (Gate::allows('isOwner') || Gate::allows('isAdmin')) {
            return view('owner');
        }
    }
}

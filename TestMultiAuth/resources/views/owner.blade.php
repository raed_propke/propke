<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @can('isAdmin')
    <div class="bg-danger">
        admin Panel
    </div>
    @endCan
    @can('isOwner')
    <div class="bg-danger">
        Owner Panel
    </div>
    @endCan
</body>
</html>
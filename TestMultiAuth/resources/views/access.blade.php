@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 class="bg-danger">HEY</h1>
        {{ Auth::user()->user_type }}
        <br>
        <form action="/access" name="accessForm" method="post">
            @csrf
            <select name="user" id="user" onchange="showUser(this)">
                @foreach ($users as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
            <table>
                <tr>
                    <th>
                        Apartment
                    </th>
                    <th>
                        Access
                    </th>
                </tr>
            @foreach ($listing as $item)
            <tr>
                <td>
                    {{ $item->some_data }}
                </td>
                <td>
                    <input type="checkbox" name="listing[]" value="{{ $item->some_data }}" id="">
                </td>
            </tr>
            @endforeach
        </table>
        <input type="submit" value="submit" class="btn btn-dark mt-2" onclick="getUser()">
    </form>
    </div>
    @endsection